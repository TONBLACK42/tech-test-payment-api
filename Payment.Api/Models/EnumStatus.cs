namespace tech_test_payment_api.Payment.Api.Models
{
    public enum EnumStatus
    {
        AguardandoPagamento =1,
        PagamentoAprovado,
        EnviandoParaTransportadora,
        Entregue,
        Cancelada
    }
}